import asyncio
import json
from logging import getLogger
from typing import Dict

import aioredis
import async_timeout
from fastapi import FastAPI
from tortoise import expand_db_url

from fastapi_router.caches import get_cache
from fastapi_router.config.database import SingleDatabaseConfig
from fastapi_router.context_injection.tenant_context import TenantContext
from fastapi_router.reactive_configurations.reactive_api import router

logger = getLogger(__name__)


class ReactiveConfigurationManager:

    reactive_configurations_endpoint = "/fast_api_tenant_router/reactive_configurations"
    reactive_configurations_change_channel = "reactive_configuration_update_fast_api_tenant_router"
    reactive_configurations_delete_channel = "reactive_configuration_delete_fast_api_tenant_router"
    request_coroutines_subscribed_for_tenant_db_config_updates = []

    async def reader(self, channel: aioredis.client.PubSub):
        while True:
            try:
                async with async_timeout.timeout(1):
                    message = await channel.get_message(ignore_subscribe_messages=True)
                    if message is not None:
                        data = message["data"]
                        data = json.loads(data)
                        logger.info(f"received a db config update request with data {data}")
                        print(f"received a db config update request with data {data}")
                        if data.get("db_config", None):
                            await self._update_tenant_db_config_for_tenant(
                                tenant_id=data["tenant_id"],
                                db_alias=data["db_alias"],
                                db_config=SingleDatabaseConfig(**data["db_config"])
                            )
                        else:
                            print("deleting")
                            await self._delete_tenant_db_config_for_tenant(tenant_id=data["tenant_id"],
                                                                           db_alias=data["db_alias"])
                    await asyncio.sleep(0.01)
            except asyncio.TimeoutError:
                print("async timeout error ")

    def __init__(self):
        self.pub_sub_cache = None

    @classmethod
    def subscribe_request_coroutines_for_tenant_db_config_updates(cls, callback):
        cls.request_coroutines_subscribed_for_tenant_db_config_updates.append(callback)

    async def _update_tenant_db_config_for_tenant(self, tenant_id, db_alias, db_config: SingleDatabaseConfig):
        tenant_context = TenantContext(tenant_id)
        from fastapi_router.database import tortoise_manager
        full_alias = tortoise_manager.create_alias_from_local_alias_and_context(
            local_alias=db_alias, context=tenant_context
        )
        info: Dict = expand_db_url(db_config.conn)

        db_config = db_config.dict(exclude={"conn"})
        info["credentials"].update(db_config)
        tortoise_manager.full_alias_to_conn_info_mapping[full_alias] = info
        logger.info(f"updated db config of {full_alias} to --->>>  {info}")
        for subscriber in self.__class__.request_coroutines_subscribed_for_tenant_db_config_updates:
            await subscriber(full_alias)

    async def initialize_reactive_configurations_api(
            self, fast_api_app: FastAPI
    ):
        fast_api_app.include_router(
            router,
            prefix=self.__class__.reactive_configurations_endpoint
        )
        self.pub_sub_cache: aioredis.Redis = get_cache()
        redis_pubsub = self.pub_sub_cache.pubsub()
        await redis_pubsub.subscribe(self.__class__.reactive_configurations_change_channel)
        await redis_pubsub.subscribe(self.__class__.reactive_configurations_delete_channel)
        asyncio.create_task(self.reader(redis_pubsub))

    @staticmethod
    async def _delete_tenant_db_config_for_tenant(tenant_id, db_alias):
        tenant_context = TenantContext(tenant_id)
        from fastapi_router.database import tortoise_manager
        full_alias = tortoise_manager.create_alias_from_local_alias_and_context(
            local_alias=db_alias, context=tenant_context
        )
        tortoise_manager.full_alias_to_conn_info_mapping.pop(full_alias)
        logger.info(f"Deleted db config of {full_alias} ")


reactive_config_manager = ReactiveConfigurationManager()


