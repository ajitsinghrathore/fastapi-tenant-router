import json

from fastapi import APIRouter

from fastapi_router.context_injection.tenant_context_tracker import tenant_context_tracker
from fastapi_router.reactive_configurations.update_api_models import UpdateResponse, UpdatePayload, DeletePayload

router = APIRouter()


@router.post(
    "/",
    response_model=UpdateResponse
)
@router.put(
    "/",
    response_model=UpdateResponse
)
async def update_db_config(payload: UpdatePayload):
    try:
        from fastapi_router import reactive_config_manager
        tenant_id = tenant_context_tracker.current().tenant_id
        await reactive_config_manager.pub_sub_cache.publish(
            "reactive_configuration_update_fast_api_tenant_router", json.dumps({
                                                            "tenant_id": tenant_id,
                                                            "db_alias": payload.db_alias,
                                                            "db_config": payload.db_config.dict()
                                                        })
        )
    except Exception as e:
        return UpdateResponse(**{
            "status": False,
            "message": f"failed to update {e}"
        })
    return UpdateResponse(**{
        "status": True,
        "message": "successfully updated"
    })


@router.delete(
    "/",
    response_model=UpdateResponse
)
async def delete_db_config(payload: DeletePayload):
    try:
        from fastapi_router import reactive_config_manager
        tenant_id = tenant_context_tracker.current().tenant_id
        await reactive_config_manager.pub_sub_cache.publish(
            "reactive_configuration_delete_fast_api_tenant_router", json.dumps({
                                                            "tenant_id": tenant_id,
                                                            "db_alias": payload.db_alias,
                                                        })
        )
    except Exception as e:
        return UpdateResponse(**{
            "status": False,
            "message": f"failed to update {e}"
        })
    return UpdateResponse(**{
        "status": True,
        "message": "successfully updated"
    })
