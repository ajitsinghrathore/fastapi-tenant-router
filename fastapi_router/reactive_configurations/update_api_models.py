from pydantic import BaseModel

from fastapi_router.config.database import SingleDatabaseConfig


class UpdateResponse(BaseModel):
    status: bool
    message: str


class UpdatePayload(BaseModel):
    db_alias: str
    db_config: SingleDatabaseConfig


class DeletePayload(BaseModel):
    db_alias: str
