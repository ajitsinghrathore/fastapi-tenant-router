from typing import Dict, Optional

from pydantic import BaseModel

from fastapi_router.config.caches import CacheSettings
from fastapi_router.config.database import DatabaseSettings
from fastapi_router.config.middleware import MiddlewareSettings
from fastapi_router.config.single_tenant_metadata import SingleTenantMetadata


class AllTenantConfig(BaseModel):
    tenants_metadata: Dict[str, SingleTenantMetadata]


class FastApiTenantRouterSettings(BaseModel):
    all_tenants_config: AllTenantConfig
    middleware_settings: Optional[MiddlewareSettings] = MiddlewareSettings()
    cache_settings: Optional[CacheSettings] = CacheSettings()
    database_settings: Optional[DatabaseSettings] = DatabaseSettings()
