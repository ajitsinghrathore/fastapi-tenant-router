import enum
from typing import Optional, Dict

from pydantic import BaseModel, validator

from fastapi_router.config.database.tortoise import TortoiseOrmSetting


class FastApiTenantRoutersORM(str, enum.Enum):
    tortoise = "tortoise"
    mongoengine = "mongoengine"


class DatabaseSettings(BaseModel):
    tortoise: Optional[TortoiseOrmSetting]


class SingleDatabaseConfig(BaseModel):
    conn: str
    minsize: Optional[int] = 0
    maxsize: Optional[int] = 5
    max_queries: Optional[int] = 50000

    # @validator("minsize")
    # def validate_min_size(cls, value):
    #     assert value == 0


class TortoiseOrmConnectionsMetadata(BaseModel):
    __root__: Dict[str, SingleDatabaseConfig]
