from typing import Dict, List, Optional

from pydantic import BaseModel


class TortoiseOrmSetting(BaseModel):
    apps: Dict[str, Dict]
    routers: Optional[List[str]] = []
    add_exception_handlers: Optional[bool] = True
    use_tz: Optional[bool] = False
    timezone: Optional[str] = "UTC"
    full_alias_prefix_key: Optional[str] = "tortoise_orm_model"
