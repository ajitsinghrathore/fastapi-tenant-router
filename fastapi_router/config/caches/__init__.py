from typing import Optional

from pydantic import BaseModel


class SingleCacheConfig(BaseModel):
    backend: str
    host: Optional[str]
    port: Optional[int]
    db: Optional[str] = 1
    pool_max_size: Optional[int] = 10
    pool_min_size: Optional[int] = 1
    conn: Optional[str]


class CacheSettings(BaseModel):
    default_cache_alias: Optional[str] = "default"
    central_cache_config: Optional[SingleCacheConfig] = None
