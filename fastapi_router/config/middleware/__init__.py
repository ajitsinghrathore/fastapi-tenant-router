from typing import Optional, Callable, List

from pydantic import BaseModel


class MiddlewareSettings(BaseModel):
    tenant_id_resolver: Optional[Callable]
    whitelist_routes: Optional[List[str]] = []
