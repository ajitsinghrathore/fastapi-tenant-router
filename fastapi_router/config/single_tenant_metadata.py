from typing import Dict, Optional

from pydantic import BaseModel

from fastapi_router.config.caches import SingleCacheConfig
from fastapi_router.config.database import TortoiseOrmConnectionsMetadata


class SingleTenantDatabases(BaseModel):
    tortoise: Optional[TortoiseOrmConnectionsMetadata]
    mongoengine: Optional[TortoiseOrmConnectionsMetadata]


class SingleTenantCaches(BaseModel):
    __root__: Dict[str, SingleCacheConfig]


class SingleTenantMetadata(BaseModel):
    databases: Optional[SingleTenantDatabases]
    caches: Optional[SingleTenantCaches]
