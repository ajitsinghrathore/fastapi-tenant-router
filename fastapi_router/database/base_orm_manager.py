from abc import ABC, abstractmethod
from typing import Dict

from fastapi_router.context_injection.tenant_context import TenantContext
from fastapi_router.context_injection.tenant_context_tracker import tenant_context_tracker


class BaseOrmManager(ABC):

    def __init__(self):
        self._full_alias_prefix_key: str | None = None

    def create_full_alias_from_local_alias(self, local_alias: str) -> str:
        tenant_context: TenantContext = tenant_context_tracker.current()
        if tenant_context:
            return self.create_alias_from_local_alias_and_context(local_alias=local_alias, context=tenant_context)

    def create_alias_from_local_alias_and_context(self, local_alias: str, context: TenantContext) -> str:
        return "{alias_prefix}@{tenant_id}@database@{alias}".format(
            alias_prefix=self._full_alias_prefix_key,
            tenant_id=context.tenant_id,
            alias=local_alias,
        )

    @abstractmethod
    def bootstrap(self, settings: any, connections: Dict[str, Dict[str, str]]) -> None:
        pass
