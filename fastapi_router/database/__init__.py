from logging import getLogger
from typing import Dict

from fastapi_router.config.all_tenant_config import AllTenantConfig
from fastapi_router.config.database import DatabaseSettings, TortoiseOrmConnectionsMetadata
from fastapi_router.config.single_tenant_metadata import SingleTenantDatabases
from fastapi_router.database.tortoise.manager import tortoise_manager

logger = getLogger(__name__)


def get_tortoise_connections_extracted(all_tenant_config: AllTenantConfig) -> Dict[str, TortoiseOrmConnectionsMetadata]:
    final_extracted_connections: Dict[str, TortoiseOrmConnectionsMetadata] = {}
    for tenant_id, tenant_metadata in all_tenant_config.tenants_metadata.items():
        databases: SingleTenantDatabases = tenant_metadata.databases
        if not databases or not databases.tortoise:
            continue
        final_extracted_connections[tenant_id] = databases.tortoise
    return final_extracted_connections


async def bootstrap_databases(settings: DatabaseSettings, all_tenants_configs: AllTenantConfig) -> None:
    logger.info("started bootstrapping of databases")
    if settings.tortoise:
        await tortoise_manager.bootstrap(settings.tortoise, get_tortoise_connections_extracted(all_tenants_configs))
    logger.info("finished bootstrapping of databases")


async def cleanup_database():
    logger.info("started cleanup of databases")
    await tortoise_manager.cleanup()
    logger.info("finished cleanup of databases")
