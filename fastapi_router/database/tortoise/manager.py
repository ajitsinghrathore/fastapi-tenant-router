import sys
from typing import Dict, Type
from unittest.mock import Mock, patch

from tortoise import Tortoise, expand_db_url, connections

from fastapi_router.config.database import TortoiseOrmSetting, TortoiseOrmConnectionsMetadata
from fastapi_router.context_injection.tenant_context import TenantContext
from fastapi_router.database.base_orm_manager import BaseOrmManager
from fastapi_router.database.tortoise.custom_connection_handler import CustomConnectionHandler
from fastapi_router.reactive_configurations import ReactiveConfigurationManager


class SameAliasButDifferentEngineError:
    pass


class TortoiseManager(BaseOrmManager):

    def __init__(self):
        super().__init__()
        self.local_alias_to_client_class_mapping: Dict[str, Type] = {}
        self.full_alias_to_conn_info_mapping: Dict[str, Dict[str, any]] = {}
        self._tortoise_config: Dict[str, any] | None = None
        self.custom_connection_handler = None

    #
    def _mocked_current_transaction_map_for_client_type(self, conn_alias):
        # only required while bootstrapping tortoise library and tortoise internally will bound each model to
        # connection so that it can set generate query sets at boot-up , but for this it only  uses the type of client
        # i.e either postgres or something other so  we will return the client type because we don't want any model to
        # tightly bound to any connection but we can say that a model is bound to local alias and when tortoise will
        # ask for that alias than our patched connection dict will internally convert it into full alias and in that
        # way tortoise will never know that a model is not bound to any connection instead it is bound to a local
        # alias . and for the very same reason we want  local alias in every tenant to use same engine as this
        # initial setup is done engine specific
        db_mock = Mock()
        db_mock.get.return_value = self.local_alias_to_client_class_mapping[conn_alias]
        return self.local_alias_to_client_class_mapping[conn_alias]

    @patch("tortoise.models.connections.get")
    # @patch("tortoise.connections.get", new_callable=MagicMock)
    async def register_tortoise(self, _patched_models_connections) -> None:
        _patched_models_connections.side_effect = self._mocked_current_transaction_map_for_client_type
        await Tortoise.init(config=self._tortoise_config)
        self.custom_connection_handler.initialization_complete()

    def _apply_patch(self):
        self.custom_connection_handler = CustomConnectionHandler()
        self.custom_connection_handler.attach_orm_manager(self)
        for module in sys.modules.values():
            if hasattr(module, "connections"):
                setattr(module, "connections", self.custom_connection_handler)

    def parse_connections_dict(self, tortoise_connections: Dict[str, TortoiseOrmConnectionsMetadata]) -> None:
        for tenant_id, orm_configs in tortoise_connections.items():
            for db_alias, conn_info in orm_configs.__root__.items():
                full_alias: str = self.create_alias_from_local_alias_and_context(db_alias, TenantContext(tenant_id))
                info: Dict = expand_db_url(conn_info.conn)

                conn_info = conn_info.dict(exclude={"conn"})
                info["credentials"].update(conn_info)
                self.full_alias_to_conn_info_mapping[full_alias] = info

                client_type: Type = connections._discover_client_class(info["engine"])
                if db_alias in self.local_alias_to_client_class_mapping:
                    assert self.local_alias_to_client_class_mapping[
                               db_alias] == client_type, SameAliasButDifferentEngineError()
                else:
                    self.local_alias_to_client_class_mapping[db_alias] = client_type

    @staticmethod
    async def cleanup():
        await Tortoise.close_connections()

    async def close_connection_after_cycle(self):
        for alias, connection_used in self.custom_connection_handler.alias_tracker.get().items():
            try:
                print("closing connection", connection_used)
                await connection_used.close()
            except BaseException as e:
                print(e, "&&&&&&&&&&&&&&&&&&&&&&&&")

    async def create_connection_before_cycle(self):
        self.custom_connection_handler.alias_tracker.set({})

    async def invalidate_full_alias(self, full_alias):
        connection_pool = self.custom_connection_handler.discard_full_alias(full_alias)
        if connection_pool:
            print("invalidating connection pool", connection_pool)
            await connection_pool.close()

    async def bootstrap(self, settings: TortoiseOrmSetting, connections: Dict[str, TortoiseOrmConnectionsMetadata]) \
            -> None:
        self._apply_patch()
        self._full_alias_prefix_key = settings.full_alias_prefix_key
        self.parse_connections_dict(connections)
        self._tortoise_config: Dict[str, any] = {
            "connections": self.full_alias_to_conn_info_mapping,
            "apps": settings.apps,
            "use_tz": settings.use_tz,
            "timezone": settings.timezone,
            "routers": settings.routers
        }

        await self.register_tortoise()
        from fastapi_router import TenantContextMiddleware
        TenantContextMiddleware.add_request_finished_coroutine(self.close_connection_after_cycle)
        TenantContextMiddleware.add_request_started_coroutine(self.create_connection_before_cycle)
        ReactiveConfigurationManager.subscribe_request_coroutines_for_tenant_db_config_updates(
            self.invalidate_full_alias)


tortoise_manager = TortoiseManager()
