import contextvars
from typing import Optional

from tortoise import BaseDBAsyncClient
from tortoise.connection import ConnectionHandler


class CustomConnectionHandler(ConnectionHandler):

    def __int__(self):
        self.orm_manager = None
        super().__init__()

    def attach_orm_manager(self, orm_manager):
        self.orm_manager = orm_manager
        self.alias_tracker = contextvars.ContextVar("alias_tracker")
        self.initialized = False

    def get(self, conn_alias: str) -> "BaseDBAsyncClient":
        full_alias = self.orm_manager.create_full_alias_from_local_alias(local_alias=conn_alias)
        if not full_alias:
            full_alias = conn_alias
        aliases_used = self.alias_tracker.get({})
        print(aliases_used, "(((((((()))))))))))))))))))", full_alias)
        if full_alias not in aliases_used:
            connection = super().get(conn_alias=full_alias)
            aliases_used[full_alias] = connection
            self.alias_tracker.set(aliases_used)
            self.discard_full_alias(full_alias)
            return connection
        else:
            return aliases_used[full_alias]

    def set(self, conn_alias: str, conn_obj: "BaseDBAsyncClient") -> contextvars.Token:
        full_alias = self.orm_manager.create_full_alias_from_local_alias(local_alias=conn_alias)
        if not full_alias:
            full_alias = conn_alias
        return super().set(conn_alias=full_alias, conn_obj=conn_obj)

    def discard(self, conn_alias: str) -> Optional["BaseDBAsyncClient"]:
        full_alias = self.orm_manager.create_full_alias_from_local_alias(local_alias=conn_alias)
        if not full_alias:
            full_alias = conn_alias
        return super().discard(conn_alias=full_alias)

    def discard_full_alias(self, full_alias):
        return super().discard(conn_alias=full_alias)

    def initialization_complete(self):
        self.initialized = True

    @property
    def db_config(self):
        if not self.initialized:
            return {}
        return self.orm_manager.full_alias_to_conn_info_mapping
