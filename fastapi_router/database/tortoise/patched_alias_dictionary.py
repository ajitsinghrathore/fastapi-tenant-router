from collections import UserDict
from logging import getLogger

from fastapi_router.database.base_orm_manager import BaseOrmManager

logger = getLogger(__name__)


class LocalAliasToFullAliasDictionary(UserDict):

    def __init__(self, manager, **kwargs):
        super().__init__(**kwargs)
        self._orm_manager: BaseOrmManager = manager

    def __getitem__(self, local_alias):
        full_alias: str = self._orm_manager.create_full_alias_from_local_alias(local_alias)
        logger.info(f"returning full alias {full_alias} created from {local_alias}")
        return self.data[full_alias]
