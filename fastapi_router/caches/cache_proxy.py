from contextvars import ContextVar
from logging import getLogger
from typing import Dict, Any

from fastapi_router.caches.backends.general_cache_backend import GeneralMultiTenantCacheBackend
from fastapi_router.caches.backends.memcached.memcached_backend import MemcachedMultiTenantAwareBackend
from fastapi_router.caches.backends.redis.redis_backend import RedisCacheMultiTenantAwareBackend
from fastapi_router.config.caches import SingleCacheConfig, CacheSettings
from fastapi_router.context_injection.tenant_context import TenantContext
from fastapi_router.context_injection.tenant_context_tracker import (
    tenant_context_tracker,
)
from fastapi_router.middleware import TenantContextMiddleware
from .backends.general_cache_connection import GeneralCacheConnectionPoolClient
from ..config.all_tenant_config import AllTenantConfig
from ..config.single_tenant_metadata import SingleTenantMetadata, SingleTenantCaches

logger = getLogger(__name__)


class CacheManager:
    """
    This class is mainly responsible for  generating full aliases
     with the help of tenant context and act as a proxy for different cache backends
    and return cache connection object  by querying cache backend .

    it is a callable , and takes  cache alias as a parameter ,
    if not given it will fetch default cache of that tenant
    """

    def __init__(self):
        self._default_connection_local_alias: str = "default"
        self._cache_backends: Dict[str, GeneralMultiTenantCacheBackend] = {}
        self._full_alias_to_cache_config_mapping: Dict[str, SingleCacheConfig] = {}
        self.central_cache_connection_pool: GeneralCacheConnectionPoolClient | None = None
        self._current_coroutine_specific_connection_pools: ContextVar[Dict[str, "GeneralCacheConnectionPoolClient"]] = \
            ContextVar("coroutine_specific_cache_connection", default={})

    def __call__(self, alias: str = "default") -> Any:
        full_alias: str = self._generate_full_alias_for_cache_connection(
            alias=alias
        )
        if full_alias is not None:
            coroutine_specific_connections = self._current_coroutine_specific_connection_pools.get()
            connection_pool = coroutine_specific_connections.get(full_alias)
            if connection_pool:
                logger.info(f"returning coroutine specific connection pool for {full_alias}")
                return connection_pool.get_internal_connection()

            cache_config: SingleCacheConfig = self._full_alias_to_cache_config_mapping.get(full_alias)
            if cache_config is not None:
                backend: GeneralMultiTenantCacheBackend = self._cache_backends[cache_config.backend]
                connection_pool_client: GeneralCacheConnectionPoolClient = backend.get_cache_pool_from_config(
                    cache_config
                )
                coroutine_specific_connections[full_alias] = connection_pool_client
                self._current_coroutine_specific_connection_pools.set(coroutine_specific_connections)
                logger.info(f"returning newly created connection pool for {full_alias}")
                return connection_pool_client.get_internal_connection()
            else:
                logger.error(
                    f"{full_alias} cache config is not present"
                )
                return None
        return self.central_cache_connection_pool.get_internal_connection()

    def _generate_full_alias_for_cache_connection(self, alias: str) -> str:
        tenant_context: TenantContext = tenant_context_tracker.current()
        if tenant_context is not None:
            return self.generate_full_alias_from_tenant_id_and_cache_alias(tenant_id=tenant_context.tenant_id,
                                                                           alias=alias)
        logger.warning("cannot generate full cache alias as there is no tenant context")

    @staticmethod
    def generate_full_alias_from_tenant_id_and_cache_alias(tenant_id: str, alias: str):
        return "{tenant_id}@cache@{alias}".format(
            tenant_id=tenant_id,
            alias=alias,
        )

    async def close_current_coroutine_connections(self):
        current_coroutine_connections = self._current_coroutine_specific_connection_pools.get()

        for connection_pool in current_coroutine_connections.values():
            try:
                await connection_pool.close_all_connections_in_pool()
            except Exception as e:
                logger.error(f"error in closing connection pool {connection_pool}  error ---->>>  {str(e)}")
        await self.initialize_context()

    async def initialize_context(self):
        self._current_coroutine_specific_connection_pools.set({})

    def _register_coroutines(self):
        TenantContextMiddleware.add_request_finished_coroutine(self.close_current_coroutine_connections)
        TenantContextMiddleware.add_request_started_coroutine(self.initialize_context)

    def _parse_connections(self, all_tenant_configs: AllTenantConfig):
        tenants_configs: Dict[str, SingleTenantMetadata] = all_tenant_configs.tenants_metadata

        for tenant_id, tenant_metadata in tenants_configs.items():
            caches: SingleTenantCaches = tenant_metadata.caches
            if not caches:
                continue
            for alias, cache_config in caches.__root__.items():
                full_alias: str = self.generate_full_alias_from_tenant_id_and_cache_alias(tenant_id, alias)
                self._full_alias_to_cache_config_mapping[full_alias] = cache_config

    async def bootstrap(self, cache_settings: CacheSettings, all_tenant_config: AllTenantConfig) -> None:
        self._default_connection_local_alias = cache_settings.default_cache_alias
        self._cache_backends["redis"] = RedisCacheMultiTenantAwareBackend()
        self._cache_backends["memcached"] = MemcachedMultiTenantAwareBackend()
        self._parse_connections(all_tenant_config)
        self._initialize_central_cache_connection(central_cache_config=cache_settings.central_cache_config)
        self._register_coroutines()

    def _initialize_central_cache_connection(self, central_cache_config: SingleCacheConfig):
        backend: GeneralMultiTenantCacheBackend = self._cache_backends[central_cache_config.backend]
        connection_pool_client: GeneralCacheConnectionPoolClient = backend.get_cache_pool_from_config(
            central_cache_config
        )
        self.central_cache_connection_pool = connection_pool_client
