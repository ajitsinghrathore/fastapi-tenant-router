from logging import getLogger

from fastapi_router.config.caches import CacheSettings
from .cache_proxy import CacheManager
from ..config.all_tenant_config import AllTenantConfig

logger = getLogger(__name__)

get_cache = CacheManager()


async def bootstrap_caches(cache_settings: CacheSettings, all_tenant_config: AllTenantConfig):
    logger.info("started bootstrapping caches")
    await get_cache.bootstrap(cache_settings, all_tenant_config)
    logger.info("completed bootstrapping caches")

# TODO memcached library has concurrency bugs and  needs to be fixed
