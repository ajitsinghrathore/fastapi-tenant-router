from abc import ABC, abstractmethod
from typing import TypeVar, Generic


class GeneralCacheConnectionPoolClient(ABC):

    def __init__(self, connection_pool):
        self._connection_pool = connection_pool

    def get_internal_connection(self):
        return self._connection_pool

    @abstractmethod
    async def close_all_connections_in_pool(self):
        pass
