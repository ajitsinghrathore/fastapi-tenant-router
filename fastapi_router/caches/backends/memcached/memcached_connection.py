import asyncio
from typing import Dict, Tuple

import aiomemcached

from fastapi_router.caches.backends.general_cache_connection import GeneralCacheConnectionPoolClient


class MemcachedConnection(GeneralCacheConnectionPoolClient):
    """
    Each MemcachedConnection is a pool of connections to a same database and whenever get_cache() is called then  it
    first goes to proxy then proxy decides the backend to contact for connection and after that , backend will return
    this connection object (pool of connections) and after every request all the pools used while life span of
    request will be closed completely
    """

    async def flush_db(self):
        await self._connection.flush_all()

    async def set_many(self, data: Dict[str, str]):
        await asyncio.gather(*[self.set(key=key, value=value) for key, value in data.items()])

    async def close_all_connections_in_pool(self):
        await self._connection.close()

    def __init__(self, cache: aiomemcached.client):
        self._connection: aiomemcached.client = cache

    async def get(self, key: str) -> str:
        answer: Tuple[bytes, any] = await self._connection.get(key.encode())
        return answer[0].decode()

    async def set(self, key: str, value: str) -> None:
        await self._connection.set(key.encode(), value.encode())

    def get_internal_connection(self):
        return self._connection

    async def close_stale_connections_in_pool(self):
        await self._connection.close()
