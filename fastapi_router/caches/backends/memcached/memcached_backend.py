import aiomemcached

from fastapi_router.caches.backends.general_cache_backend import GeneralMultiTenantCacheBackend
from fastapi_router.caches.backends.general_cache_connection import GeneralCacheConnectionPoolClient
from fastapi_router.caches.backends.memcached.memcached_connection import MemcachedConnection
from fastapi_router.config.caches import SingleCacheConfig


class MemcachedMultiTenantAwareBackend(GeneralMultiTenantCacheBackend):

    def __init__(self):
        super().__init__()

    def get_cache_pool_from_config(self, cache_config: SingleCacheConfig) -> GeneralCacheConnectionPoolClient:

        if cache_config.conn is None:
            url: str = f"memcached://{cache_config.host}:{cache_config.port}"
        else:
            url: str = cache_config.conn

        cache: aiomemcached.client = aiomemcached.Client(uri=url,
                                                         pool_maxsize=cache_config.pool_max_size,
                                                         pool_minsize=cache_config.pool_min_size)
        return MemcachedConnection(cache=cache)
