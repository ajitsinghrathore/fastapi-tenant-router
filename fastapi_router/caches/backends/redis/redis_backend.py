from logging import getLogger

import aioredis

from fastapi_router.caches.backends.general_cache_backend import GeneralMultiTenantCacheBackend
from fastapi_router.caches.backends.general_cache_connection import GeneralCacheConnectionPoolClient
from fastapi_router.caches.backends.redis.redis_connection import RedisConnectionPoolClient
from fastapi_router.config.caches import SingleCacheConfig

logger = getLogger(__name__)


class RedisCacheMultiTenantAwareBackend(GeneralMultiTenantCacheBackend):

    def get_cache_pool_from_config(self, cache_config: SingleCacheConfig) -> GeneralCacheConnectionPoolClient:
        if cache_config.conn is None:
            url: str = f"redis://{cache_config.host}:{cache_config.port}/{cache_config.db}"
        else:
            url: str = cache_config.conn
        cache: aioredis.Redis = aioredis.from_url(url, max_connections=cache_config.pool_max_size,
                                                  decode_responses=True)
        return RedisConnectionPoolClient(cache)
