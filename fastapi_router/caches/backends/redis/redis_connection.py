import aioredis

from fastapi_router.caches.backends.general_cache_connection import GeneralCacheConnectionPoolClient


class RedisConnectionPoolClient(GeneralCacheConnectionPoolClient):

    def __init__(self, redis: aioredis.Redis):
        super().__init__(redis)

    async def close_all_connections_in_pool(self):
        await self.get_internal_connection().connection_pool.disconnect(inuse_connections=True)
