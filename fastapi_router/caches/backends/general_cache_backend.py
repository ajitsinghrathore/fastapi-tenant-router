from abc import ABC, abstractmethod
from logging import getLogger

from fastapi_router.caches.backends.general_cache_connection import GeneralCacheConnectionPoolClient
from fastapi_router.config.caches import SingleCacheConfig

logger = getLogger(__name__)


class GeneralMultiTenantCacheBackend(ABC):

    @abstractmethod
    def get_cache_pool_from_config(self, cache_config: SingleCacheConfig) -> GeneralCacheConnectionPoolClient:
        pass
