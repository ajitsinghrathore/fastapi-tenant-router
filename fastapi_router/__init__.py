import time
from logging import getLogger
from typing import Optional

from fastapi.applications import FastAPI

from fastapi_router.config.all_tenant_config import (
    FastApiTenantRouterSettings,
)
from .caches import bootstrap_caches
from .database import bootstrap_databases, cleanup_database
from .middleware import TenantContextMiddleware
from .reactive_configurations import reactive_config_manager

logger = getLogger(__name__)


class _LibraryAppLinker:
    def __init__(self):
        self._app: Optional[FastAPI] = None
        self._settings: Optional[FastApiTenantRouterSettings] = None

    def __call__(self, app: FastAPI, settings: FastApiTenantRouterSettings):
        self._app = app
        self._settings = settings
        app.add_event_handler("startup", self._bootstrap)
        app.add_event_handler("shutdown", self._cleanup)

    async def _bootstrap(self):
        logger.info("started bootstrapping of fastapi tenant router")
        start_time = time.time()
        self._app.add_middleware(
            TenantContextMiddleware,
            middleware_settings=self._settings.middleware_settings,
        )
        await bootstrap_caches(self._settings.cache_settings, self._settings.all_tenants_config)
        await bootstrap_databases(self._settings.database_settings, self._settings.all_tenants_config)
        await reactive_config_manager.initialize_reactive_configurations_api(fast_api_app=self._app)
        logger.info(f"total time for boot-up {time.time() - start_time}")

    @staticmethod
    async def _cleanup():
        await cleanup_database()


fastapi_multi_tenant_router_set_app = _LibraryAppLinker()
