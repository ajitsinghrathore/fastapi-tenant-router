import logging
from typing import Any, Callable, List

from fastapi import Request, Response
from fastapi.applications import FastAPI
from starlette.middleware.base import BaseHTTPMiddleware

from fastapi_router.config.middleware import MiddlewareSettings
from fastapi_router.context_injection.context_injector import TenantContextBind
from fastapi_router.context_injection.tenant_context import TenantContext

logger = logging.getLogger(__name__)


class TenantIdResolverException(Exception):
    pass


class TenantContextMiddleware(BaseHTTPMiddleware):
    # these callbacks must be async in nature
    _coroutines_for_request_finished: List[Callable] = []
    _coroutines_for_request_started: List[Callable] = []

    def __init__(self, app: FastAPI, middleware_settings: MiddlewareSettings):
        super().__init__(app)
        self.tenant_id_resolver: Callable = (
            middleware_settings.tenant_id_resolver
        )
        self.whitelist_routes: List[str] = middleware_settings.whitelist_routes

    def _extract_tenant_id(self, request: Request) -> str:
        if self.tenant_id_resolver:
            return self.tenant_id_resolver(request)

        tenant_id: str = request.headers.get("x-tenant-id", None)
        if tenant_id:
            return tenant_id
        else:
            logger.error(
                "can not resolve tenant id from headers in request."
                " also custom callable for resolving tenant id was not given "
            )
            raise TenantIdResolverException()

    def _check_whitelist(self, request: Request):
        return request.scope["path"] in self.whitelist_routes

    async def _trigger_pre_request_coroutines(self) -> None:
        logger.info("triggering pre request coroutines")
        for coroutine in self.__class__._coroutines_for_request_started:
            await coroutine()

    async def _trigger_post_request_coroutines(self) -> None:
        logger.info("triggering post request coroutines")
        for coroutine in self.__class__._coroutines_for_request_finished:
            await coroutine()

    @classmethod
    def add_request_finished_coroutine(cls, coroutine: Any):
        cls._coroutines_for_request_finished.append(coroutine)

    @classmethod
    def add_request_started_coroutine(cls, coroutine: Any):
        cls._coroutines_for_request_started.append(coroutine)

    async def dispatch(self, request: Request, call_next: Any) -> Response:
        await self._trigger_pre_request_coroutines()
        if self._check_whitelist(request=request):
            logger.info("this request is whitelisted")
            response = await call_next(request)
        else:
            tenant_id: str = self._extract_tenant_id(request=request)
            logger.info(f"{tenant_id} resolved in request")
            with TenantContextBind(TenantContext(tenant_id=tenant_id)):
                response = await call_next(request)
        await self._trigger_post_request_coroutines()

        return response
