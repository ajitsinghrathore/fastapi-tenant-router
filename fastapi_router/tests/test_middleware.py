import logging
from unittest.mock import patch, Mock, AsyncMock, call

import pytest

from fastapi_router import TenantContextMiddleware
from fastapi_router.middleware import TenantIdResolverException


class TestTenantContextMiddleware:

    @pytest.fixture
    def middleware(self):
        return TenantContextMiddleware(app=Mock(), middleware_settings=Mock())

    @staticmethod
    def test_constructor():
        mocked_middleware_settings = Mock()
        mocked_app = Mock()
        middleware = TenantContextMiddleware(mocked_app, middleware_settings=mocked_middleware_settings)

        assert middleware.tenant_id_resolver == mocked_middleware_settings.tenant_id_resolver
        assert middleware.whitelist_routes == mocked_middleware_settings.whitelist_routes

    def test_extract_tenant_id_from_resolver(self, middleware):
        request = Mock()
        middleware.tenant_id_resolver.return_value = "extracted_tenant_id"

        tenant_id = middleware._extract_tenant_id(request=request)

        middleware.tenant_id_resolver.assert_called_once_with(request)
        assert tenant_id == "extracted_tenant_id"

    def test_extract_tenant_id_from_header_present(self, middleware):
        request = Mock()
        request.headers.get.return_value = "headers_tenant_id"
        middleware.tenant_id_resolver = None

        tenant_id = middleware._extract_tenant_id(request=request)
        request.headers.get.assert_called_once_with("x-tenant-id", None)
        assert tenant_id == "headers_tenant_id"

    def test_extract_tenant_id_from_header_absent(self, middleware, caplog):
        request = Mock()
        request.headers.get.return_value = None
        middleware.tenant_id_resolver = None
        try:
            middleware._extract_tenant_id(request=request)
            assert False
        except TenantIdResolverException:
            pass
        request.headers.get.assert_called_once_with("x-tenant-id", None)
        assert "can not resolve tenant id from headers in request." \
               " also custom callable for resolving tenant id was not given " in caplog.text

    def test_whitelist_routes(self, middleware):
        request = Mock()
        request.scope = {"path": "url_path"}
        middleware.whitelist_routes = ["url_path"]

        is_whitelisted = middleware._check_whitelist(request)

        assert is_whitelisted

    @pytest.mark.parametrize("scope,whitelist_routes", [
        ({"path": "url_path"}, []),
        ({"path": "url_path"}, ["url_path_other"]),
    ])
    def test_whitelist_routes_returning_false(self, middleware, scope, whitelist_routes):
        request = Mock()
        request.scope = scope
        middleware.whitelist_routes = whitelist_routes

        is_whitelisted = middleware._check_whitelist(request)

        assert not is_whitelisted

    @pytest.mark.anyio
    async def test_trigger_pre_request_coroutines(self, middleware, caplog):
        caplog.set_level(logging.INFO)
        coroutine_1 = AsyncMock(return_value="completed 1")
        coroutine_2 = AsyncMock(return_value="completed 2")

        # coroutine used to test order in which all coroutines are called
        manager = Mock()
        manager.attach_mock(coroutine_1, "coroutine_1")
        manager.attach_mock(coroutine_2, "coroutine_2")

        middleware.__class__._coroutines_for_request_started = [coroutine_1, coroutine_2]

        await middleware._trigger_pre_request_coroutines()

        assert "triggering pre request coroutines" in caplog.text
        assert manager.mock_calls == [call.coroutine_1(), call.coroutine_2()]

    @pytest.mark.anyio
    async def test_trigger_post_request_coroutines(self, middleware, caplog):
        caplog.set_level(logging.INFO)
        coroutine_1 = AsyncMock(return_value="completed 1")
        coroutine_2 = AsyncMock(return_value="completed 2")

        # coroutine used to test order in which all coroutines are called
        manager = Mock()
        manager.attach_mock(coroutine_1, "coroutine_1")
        manager.attach_mock(coroutine_2, "coroutine_2")

        middleware.__class__._coroutines_for_request_finished = [coroutine_1, coroutine_2]

        await middleware._trigger_post_request_coroutines()

        assert "triggering post request coroutines" in caplog.text
        assert manager.mock_calls == [call.coroutine_1(), call.coroutine_2()]

    def test_add_request_finished_coroutine(self):
        TenantContextMiddleware._coroutines_for_request_finished = ["coroutine_1"]
        new_coroutine = AsyncMock()
        TenantContextMiddleware.add_request_finished_coroutine(new_coroutine)

        assert TenantContextMiddleware._coroutines_for_request_finished == ["coroutine_1", new_coroutine]

    def test_add_request_started_coroutine(self):
        TenantContextMiddleware._coroutines_for_request_started = ["coroutine_1"]
        new_coroutine = AsyncMock()
        TenantContextMiddleware.add_request_started_coroutine(new_coroutine)

        assert TenantContextMiddleware._coroutines_for_request_started == ["coroutine_1", new_coroutine]

    @pytest.mark.anyio
    @patch("fastapi_router.middleware.TenantContextBind")
    @patch("fastapi_router.middleware.TenantContext", return_value="tenant_context")
    @patch("fastapi_router.middleware.TenantContextMiddleware._extract_tenant_id", return_value="tenant_id")
    @patch("fastapi_router.middleware.TenantContextMiddleware._check_whitelist", return_value=True)
    @patch("fastapi_router.middleware.TenantContextMiddleware._trigger_post_request_coroutines")
    @patch("fastapi_router.middleware.TenantContextMiddleware._trigger_pre_request_coroutines")
    async def test_dispatch_for_whitelist_routes(self,
                                                 patched_pre_request_trigger,
                                                 patched_post_request_trigger,
                                                 patched_check_whitelist,
                                                 patched_extract_tenant_id,
                                                 patched_tenant_context,
                                                 patched_tenant_context_bind,
                                                 middleware,
                                                 caplog):
        caplog.set_level(logging.INFO)
        request = Mock()
        call_next = AsyncMock(return_value="response_received")
        real_response = await middleware.dispatch(request=request, call_next=call_next)

        patched_check_whitelist.assert_called_once_with(request=request)

        patched_extract_tenant_id.assert_not_called()
        patched_tenant_context.assert_not_called()
        patched_tenant_context_bind.return_value.__enter__.assert_not_called()
        patched_tenant_context_bind.return_value.__exit__.assert_not_called()

        call_next.assert_called_once_with(request)

        assert "this request is whitelisted" in caplog.text
        assert real_response == "response_received"

    @pytest.mark.anyio
    @patch("fastapi_router.middleware.TenantContextBind")
    @patch("fastapi_router.middleware.TenantContext", return_value="tenant_context")
    @patch("fastapi_router.middleware.TenantContextMiddleware._extract_tenant_id", return_value="tenant_id")
    @patch("fastapi_router.middleware.TenantContextMiddleware._check_whitelist", return_value=False)
    @patch("fastapi_router.middleware.TenantContextMiddleware._trigger_post_request_coroutines")
    @patch("fastapi_router.middleware.TenantContextMiddleware._trigger_pre_request_coroutines")
    async def test_dispatch_for_non_whitelist_routes(self,
                                                     patched_pre_request_trigger,
                                                     patched_post_request_trigger,
                                                     patched_check_whitelist,
                                                     patched_extract_tenant_id,
                                                     patched_tenant_context,
                                                     patched_tenant_context_bind,
                                                     middleware,
                                                     caplog):
        caplog.set_level(logging.INFO)
        request = Mock()
        call_next = AsyncMock(return_value="response_received")
        real_response = await middleware.dispatch(request=request, call_next=call_next)

        patched_pre_request_trigger.assert_called_once()
        patched_post_request_trigger.assert_called_once()

        patched_check_whitelist.assert_called_once_with(request=request)

        patched_extract_tenant_id.assert_called_once_with(request=request)
        patched_tenant_context.assert_called_once_with(tenant_id="tenant_id")
        patched_tenant_context_bind.return_value.__enter__.assert_called_once()
        patched_tenant_context_bind.return_value.__exit__.assert_called_once()

        call_next.assert_called_once_with(request)

        assert "tenant_id resolved in request" in caplog.text
        assert real_response == "response_received"
