import logging
from unittest.mock import patch, AsyncMock, Mock, MagicMock, call

import pytest

from fastapi_router.caches import CacheManager


class TestCacheManager:

    @pytest.fixture
    def proxy(self):
        return CacheManager()

    def test_constructor(self):
        proxy = CacheManager()

        assert proxy._default_connection_local_alias == "default"
        assert proxy._cache_backends == {}
        assert proxy._full_alias_to_cache_config_mapping == {}

    @pytest.mark.parametrize("alias", ["alias_1", None])
    @patch(
        "fastapi_router.caches.cache_proxy.CacheManager._generate_full_alias_for_cache_connection",
        return_value="full_resolved_alias")
    def test_call_with_config_present(
            self,
            patched_generate_full_alias_for_cache_connection,
            alias,
            caplog,
            proxy):
        caplog.set_level(logging.INFO)
        single_cache_config = Mock()
        full_alias_to_cache_config_mock = Mock()
        full_alias_to_cache_config_mock.get.return_value = single_cache_config
        proxy._full_alias_to_cache_config_mapping = full_alias_to_cache_config_mock

        backend_mock = Mock()
        connection_mock = Mock()
        backend_mock.get_connection.return_value = connection_mock
        proxy._cache_backends = MagicMock()
        proxy._cache_backends.__getitem__.return_value = backend_mock
        if not alias:
            conn = proxy()
        else:
            conn = proxy(alias=alias)

        patched_generate_full_alias_for_cache_connection.assert_called_once_with(alias=alias if alias else "default")
        full_alias_to_cache_config_mock.get.assert_called_once_with("full_resolved_alias", None)
        proxy._cache_backends.__getitem__.assert_called_once_with(single_cache_config.backend)
        backend_mock.get_connection.assert_called_once_with(single_cache_config, "full_resolved_alias")
        assert conn is connection_mock
        assert "returning cache connection pool for full_resolved_alias" in caplog.text

    @pytest.mark.parametrize("alias", ["alias_1", None])
    @patch(
        "fastapi_router.caches.cache_proxy.CacheManager._generate_full_alias_for_cache_connection",
        return_value="full_resolved_alias")
    def test_call_without_config_present(
            self,
            patched_generate_full_alias_for_cache_connection,
            caplog,
            alias,
            proxy):
        full_alias_to_cache_config_mock = Mock()
        full_alias_to_cache_config_mock.get.return_value = None
        proxy._full_alias_to_cache_config_mapping = full_alias_to_cache_config_mock
        if not alias:
            proxy()
        else:
            proxy(alias=alias)

        patched_generate_full_alias_for_cache_connection.assert_called_once_with(alias=alias if alias else "default")
        full_alias_to_cache_config_mock.get.assert_called_once_with("full_resolved_alias", None)

        assert "full_resolved_alias cache config is not present" in caplog.text

    @patch("fastapi_router.caches.cache_proxy.CacheManager.generate_full_alias_from_tenant_id_and_cache_alias",
           return_value="returned_full_alias")
    @patch("fastapi_router.caches.cache_proxy.tenant_context_tracker")
    def test_generate_full_alias_for_cache_connection_successfully(
            self,
            patched_tenant_context_tracker,
            patched_generate_full_alias_from_tenant_id_and_cache_alias,
            proxy):

        tenant_context_mock = Mock()
        patched_tenant_context_tracker.current.return_value = tenant_context_mock
        full_alias = proxy._generate_full_alias_for_cache_connection("alias_input")

        patched_tenant_context_tracker.current.assert_called_once_with()
        patched_generate_full_alias_from_tenant_id_and_cache_alias.assert_called_once_with(
            tenant_id=tenant_context_mock.tenant_id, alias="alias_input")

        assert full_alias == "returned_full_alias"

    @patch("fastapi_router.caches.cache_proxy.CacheManager.generate_full_alias_from_tenant_id_and_cache_alias")
    @patch("fastapi_router.caches.cache_proxy.tenant_context_tracker")
    def test_generate_full_alias_for_cache_connection_unsuccessfully(
            self,
            patched_tenant_context_tracker,
            patched_generate_full_alias_from_tenant_id_and_cache_alias,
            caplog,
            proxy):

        patched_tenant_context_tracker.current.return_value = None
        full_alias = proxy._generate_full_alias_for_cache_connection("alias_input")

        patched_tenant_context_tracker.current.assert_called_once_with()
        patched_generate_full_alias_from_tenant_id_and_cache_alias.assert_not_called()

        assert not full_alias
        assert "cannot generate full cache alias as there is no tenant context" in caplog.text

    def test_generate_full_alias_from_tenant_id_and_cache_alias(self, proxy):
        full_alias = proxy.generate_full_alias_from_tenant_id_and_cache_alias(alias="input_alias",
                                                                              tenant_id="input_tenant_id")

        assert full_alias == "input_tenant_id@cache@input_alias"

    @patch("fastapi_router.caches.cache_proxy.TenantContextMiddleware")
    def test_register_coroutines(self, patched_middleware, proxy):
        backend_1 = Mock()
        backend_2 = Mock()
        proxy._cache_backends = {"backend1": backend_1, "backend2": backend_2}

        proxy._register_coroutines()

        expected_registers_calls = [
            call.add_request_finished_coroutine(backend_1.close_all_stale_connections),
            call.add_request_started_coroutine(backend_1.initialize_context),
            call.add_request_finished_coroutine(backend_2.close_all_stale_connections),
            call.add_request_started_coroutine(backend_2.initialize_context)
        ]

        patched_middleware.assert_has_calls(expected_registers_calls)

    @pytest.mark.anyio
    async def test_cleanup(self, proxy):
        backend_1 = AsyncMock()
        backend_2 = AsyncMock()
        proxy._cache_backends = {"backend1": backend_1, "backend2": backend_2}

        await proxy.cleanup()

        backend_1.terminate_all_connections.assert_awaited_once_with()
        backend_2.terminate_all_connections.assert_awaited_once_with()

    def test_parse_connection(self):
        pass  # TODO

    @pytest.mark.anyio
    @patch("fastapi_router.caches.cache_proxy.CacheManager._parse_connections")
    @patch("fastapi_router.caches.cache_proxy.CacheManager._register_coroutines")
    @patch("fastapi_router.caches.cache_proxy.MemcachedMultiTenantAwareBackend")
    @patch("fastapi_router.caches.cache_proxy.RedisCacheMultiTenantAwareBackend")
    async def test_bootstrap(
            self,
            patched_redis,
            patched_memcached,
            patched_register_coroutines,
            patched_parse_connections,
            proxy

    ):
        redis_backend_mock = Mock()
        memcached_backend_mock = Mock()
        patched_memcached.return_value = memcached_backend_mock
        patched_redis.return_value = redis_backend_mock

        cache_settings = Mock()
        all_tenant_config = Mock()

        await proxy.bootstrap(cache_settings, all_tenant_config)

        assert proxy._cache_backends == {"redis": redis_backend_mock, "memcached": memcached_backend_mock}
        patched_register_coroutines.assert_called_once_with()
        patched_parse_connections.assert_called_once_with(all_tenant_config)
