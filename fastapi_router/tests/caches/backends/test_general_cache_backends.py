import logging
from unittest.mock import patch, Mock, MagicMock, AsyncMock

import pytest

from fastapi_router.caches.backends.general_cache_backend import GeneralMultiTenantCacheBackend


class TestGeneralMultiTenantCacheBackend:

    @pytest.fixture
    @patch("fastapi_router.caches.backends.general_cache_backend.ContextVar")
    def general_cache_backend(self, patched_context_var):
        GeneralMultiTenantCacheBackend.__abstractmethods__ = set()
        return GeneralMultiTenantCacheBackend("backend_name")

    @patch("fastapi_router.caches.backends.general_cache_backend.ContextVar", return_value="patched_context_var")
    def test_constructor(self, patched_context_var):
        GeneralMultiTenantCacheBackend.__abstractmethods__ = set()
        backend = GeneralMultiTenantCacheBackend(backend_name="input_name")

        assert backend._backend_name == "input_name"
        assert backend._cache_pools == {}
        assert backend._used_alias_tracker == "patched_context_var"

        patched_context_var.assert_called_once_with("used_alias_tracker_input_name")

    @pytest.mark.anyio
    def test_get_connection_from_already_present_connection(self, general_cache_backend, caplog):
        caplog.set_level(logging.INFO)
        used_alias_tracker = Mock()
        used_alias_tracker.get.return_value = ["used_aliases_list"]
        general_cache_backend._used_alias_tracker = used_alias_tracker

        cache_config = Mock()
        cache_pools = MagicMock()
        cache_pools.__contains__.return_value = True
        general_cache_backend._cache_pools = cache_pools

        connection = general_cache_backend.get_connection(cache_config, full_alias="full_input_alias")

        used_alias_tracker.set.assert_called_once_with(["used_aliases_list", "full_input_alias"])

        cache_pools.__contains__.assert_called_once_with("full_input_alias")

        cache_pools.__getitem__.assert_called_once_with("full_input_alias")
        assert connection == cache_pools["full_input_alias"]
        assert "reusing already present connection pool for alias full_input_alias" in caplog.text

    @pytest.mark.anyio
    def test_get_connection_when_connection_is_not_present(self, general_cache_backend):
        used_alias_tracker = Mock()
        used_alias_tracker.get.return_value = ["used_aliases_list"]
        general_cache_backend._used_alias_tracker = used_alias_tracker

        cache_config = Mock()
        cache_pools = MagicMock()
        cache_pools.__contains__.return_value = False
        general_cache_backend._cache_pools = cache_pools

        general_cache_backend.get_cache_pool_from_config = Mock(return_value="new_conn")

        connection = general_cache_backend.get_connection(cache_config, full_alias="full_input_alias")

        used_alias_tracker.set.assert_called_once_with(["used_aliases_list", "full_input_alias"])
        cache_pools.__contains__.assert_called_once_with("full_input_alias")

        cache_pools.__setitem__.assert_called_once_with("full_input_alias", "new_conn")
        general_cache_backend.get_cache_pool_from_config.assert_called_once_with(cache_config)

        assert connection == "new_conn"

    @pytest.mark.anyio
    @patch(
        "fastapi_router.caches.backends.general_cache_backend.GeneralMultiTenantCacheBackend"
        "._all_cache_pools_used_in_this_context")
    @patch("fastapi_router.caches.backends.general_cache_backend.asyncio")
    async def test_close_all_stale_connections(
            self,
            patched_asyncio,
            patched_all_cache_pools,
            general_cache_backend,
            caplog
    ):
        caplog.set_level(logging.INFO)
        cache_1 = Mock()
        cache_1.close_stale_connections_in_pool.return_value = "close_cache_1_coroutine"
        cache_2 = Mock()
        cache_2.close_stale_connections_in_pool.return_value = "close_cache_2_coroutine"
        patched_all_cache_pools.return_value = {"cache_1": cache_1, "cache_2": cache_2}.items()

        patched_asyncio.gather = AsyncMock()

        await general_cache_backend.close_all_stale_connections()

        cache_1.close_stale_connections_in_pool.assert_called_once_with()
        cache_2.close_stale_connections_in_pool.assert_called_once_with()

        patched_asyncio.gather.assert_awaited_with("close_cache_1_coroutine", "close_cache_2_coroutine")

        assert "closed all stale connections in backend_name cache pools" in caplog.text

    @pytest.mark.anyio
    @patch(
        "fastapi_router.caches.backends.general_cache_backend.GeneralMultiTenantCacheBackend."
        "close_all_stale_connections")
    async def test_initialize_context(self, patched_close_all, general_cache_backend):
        general_cache_backend._used_alias_tracker = Mock()

        await general_cache_backend.initialize_context()

        patched_close_all.assert_awaited_once_with()
        general_cache_backend._used_alias_tracker.set.assert_called_once_with([])

    def test_all_cache_pools(self, general_cache_backend):
        all_caches = Mock()
        all_caches.items.return_value = "items_view_of_all_caches"
        general_cache_backend._cache_pools = all_caches

        pools = general_cache_backend.all_cache_pools()

        all_caches.items.assert_called_once_with()

        assert pools == "items_view_of_all_caches"
