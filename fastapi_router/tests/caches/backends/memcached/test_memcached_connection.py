from unittest.mock import Mock, AsyncMock, MagicMock, patch, call

import pytest

from fastapi_router.caches.backends.memcached.memcached_connection import MemcachedConnection


class TestMemcachedConnection:

    @pytest.fixture
    def memcached_connection(self):
        connection = AsyncMock()
        return MemcachedConnection(connection), connection

    def test_constructor(self):
        cache = Mock()
        obj = MemcachedConnection(cache=cache)
        assert obj._connection is cache

    @pytest.mark.anyio
    async def test_flush_db(self, memcached_connection):
        await memcached_connection[0].flush_db()
        memcached_connection[1].flush_all.assert_awaited_once_with()

    @pytest.mark.anyio
    async def test_close_all_connections_in_pool(self, memcached_connection):
        await memcached_connection[0].close_all_connections_in_pool()
        memcached_connection[1].close.assert_awaited_once_with()

    @pytest.mark.anyio
    async def test_get(self, memcached_connection):
        key = Mock()
        key.encode.return_value = "encoded_key"
        tuple_from_get_call = MagicMock()
        tuple_from_get_call[0].decode.return_value = "final_value"
        memcached_connection[1].get.return_value = tuple_from_get_call

        value = await memcached_connection[0].get(key)

        key.encode.assert_called_once_with()
        memcached_connection[1].get.assert_awaited_once_with("encoded_key")
        tuple_from_get_call[0].decode.assert_called_once_with()
        assert value == "final_value"

    @pytest.mark.anyio
    async def test_set(self, memcached_connection):
        key = Mock()
        key.encode.return_value = "encoded_key"
        value = Mock()
        value.encode.return_value = "encoded_value"

        await memcached_connection[0].set(key, value)

        key.encode.assert_called_once_with()
        value.encode.assert_called_once_with()
        memcached_connection[1].set.assert_awaited_once_with("encoded_key", "encoded_value")

    def test_get_internal_connection(self, memcached_connection):
        conn = memcached_connection[0].get_internal_connection()
        assert conn is memcached_connection[1]

    @pytest.mark.anyio
    async def test_close_stale_connections_in_pool(self, memcached_connection):
        await memcached_connection[0].close_stale_connections_in_pool()
        memcached_connection[1].close.assert_awaited_once_with()

    @pytest.mark.anyio
    @patch("fastapi_router.caches.backends.memcached.memcached_connection.MemcachedConnection.set", new_callable=Mock)
    @patch("fastapi_router.caches.backends.memcached.memcached_connection.asyncio", new_callable=AsyncMock)
    async def test_set_many(self, patched_asyncio, patched_set_call, memcached_connection):
        data = Mock()
        data.items.return_value = [("key1", "value1"), ("key2", "value2")]
        expected_set_calls = [call(key="key1", value="value1"), call(key="key2", value="value2")]
        expected_set_calls_coroutines = ["coroutine_1", "coroutine_2"]
        patched_set_call.side_effect = expected_set_calls_coroutines

        await memcached_connection[0].set_many(data)

        patched_set_call.assert_has_calls(expected_set_calls, any_order=True)
        patched_asyncio.gather.assert_awaited_once_with("coroutine_1", "coroutine_2")
