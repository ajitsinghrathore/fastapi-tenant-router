from unittest.mock import patch, Mock

import pytest

from fastapi_router.caches.backends.memcached.memcached_backend import MemcachedMultiTenantAwareBackend


class TestMemcachedMultiTenantAwareBackend:

    @pytest.fixture
    @patch("fastapi_router.caches.backends.memcached.memcached_backend.GeneralMultiTenantCacheBackend.__init__")
    def memcached_backend(self, patched_super_constructor):
        return MemcachedMultiTenantAwareBackend()

    @patch("fastapi_router.caches.backends.memcached.memcached_backend.GeneralMultiTenantCacheBackend.__init__")
    def test_constructor(self, patched_super_constructor):
        MemcachedMultiTenantAwareBackend()
        patched_super_constructor.assert_called_once_with("memcached")

    @pytest.mark.anyio
    @patch("fastapi_router.caches.backends.memcached.memcached_backend.MemcachedConnection")
    @patch("fastapi_router.caches.backends.memcached.memcached_backend.aiomemcached")
    def test_get_cache_from_config_from_conn_string(self,
                                                    patched_aiomemcached,
                                                    patched_memcached_connection,
                                                    memcached_backend):
        cache_config = Mock()
        cache_config.conn = "connection_string"

        raw_aiomemcached = Mock()
        patched_aiomemcached.Client.return_value = raw_aiomemcached

        patched_memcached_connection.return_value = "wrapped_connection_object"

        returned_conn = memcached_backend.get_cache_pool_from_config(cache_config=cache_config)

        patched_aiomemcached.Client.assert_called_once_with(uri="connection_string",
                                                            pool_maxsize=cache_config.pool_max_size,
                                                            pool_minsize=cache_config.pool_min_size)

        patched_memcached_connection.assert_called_once_with(cache=raw_aiomemcached)

        assert returned_conn == "wrapped_connection_object"

    @pytest.mark.anyio
    @patch("fastapi_router.caches.backends.memcached.memcached_backend.MemcachedConnection")
    @patch("fastapi_router.caches.backends.memcached.memcached_backend.aiomemcached")
    def test_get_cache_from_config_without_conn_string(self,
                                                       patched_aiomemcached,
                                                       patched_memcached_connection,
                                                       memcached_backend):
        cache_config = Mock()
        cache_config.conn = None
        cache_config.host = "patched_host"
        cache_config.port = "patched_port"

        expected_connection_string = f"memcached://{cache_config.host}:{cache_config.port}"

        raw_aiomemcached = Mock()
        patched_aiomemcached.Client.return_value = raw_aiomemcached

        patched_memcached_connection.return_value = "wrapped_connection_object"

        returned_conn = memcached_backend.get_cache_pool_from_config(cache_config=cache_config)

        patched_aiomemcached.Client.assert_called_once_with(uri=expected_connection_string,
                                                            pool_maxsize=cache_config.pool_max_size,
                                                            pool_minsize=cache_config.pool_min_size)

        patched_memcached_connection.assert_called_once_with(cache=raw_aiomemcached)

        assert returned_conn == "wrapped_connection_object"
