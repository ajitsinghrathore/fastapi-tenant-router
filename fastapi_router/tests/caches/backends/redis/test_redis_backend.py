from unittest.mock import patch, Mock

import pytest

from fastapi_router.caches.backends.redis.redis_backend import RedisCacheMultiTenantAwareBackend


class TestRedisCacheMultiTenantAwareBackend:

    @pytest.fixture
    @patch("fastapi_router.caches.backends.redis.redis_backend.GeneralMultiTenantCacheBackend.__init__")
    def redis_backend(self, _):
        return RedisCacheMultiTenantAwareBackend()

    @patch("fastapi_router.caches.backends.redis.redis_backend.GeneralMultiTenantCacheBackend.__init__")
    def test_constructor(self, patched_super_constructor):
        RedisCacheMultiTenantAwareBackend()
        patched_super_constructor.assert_called_once_with("redis")

    @pytest.mark.anyio
    @patch("fastapi_router.caches.backends.redis.redis_backend.RedisConnectionPoolClient")
    @patch("fastapi_router.caches.backends.redis.redis_backend.aioredis")
    def test_get_cache_from_config_from_conn_string(self, patched_aioredis, patched_redis_conn, redis_backend):
        cache_config = Mock()
        cache_config.conn = "mocked_conn_url"
        patched_aioredis.from_url = Mock(return_value="connection_object")
        patched_redis_conn.return_value = "final_wrapped_conn_object"

        conn = redis_backend.get_cache_pool_from_config(cache_config)

        patched_aioredis.from_url.assert_called_once_with("mocked_conn_url",
                                                          max_connections=cache_config.pool_max_size,
                                                          decode_responses=True)
        patched_redis_conn.assert_called_once_with("connection_object")
        assert conn == "final_wrapped_conn_object"

    @pytest.mark.anyio
    @patch("fastapi_router.caches.backends.redis.redis_backend.RedisConnectionPoolClient")
    @patch("fastapi_router.caches.backends.redis.redis_backend.aioredis")
    def test_get_cache_from_config_without_conn_string(self, patched_aioredis, patched_redis_conn, redis_backend):
        cache_config = Mock()
        cache_config.conn = None
        cache_config.host = "patched_host"
        cache_config.port = "patched_port"
        cache_config.db = "patched_db"

        patched_aioredis.from_url = Mock(return_value="connection_object")
        patched_redis_conn.return_value = "final_wrapped_conn_object"

        expected_url_constructed = f"redis://{cache_config.host}:{cache_config.port}/{cache_config.db}"

        conn = redis_backend.get_cache_pool_from_config(cache_config)

        patched_aioredis.from_url.assert_called_once_with(expected_url_constructed,
                                                          max_connections=cache_config.pool_max_size,
                                                          decode_responses=True)
        patched_redis_conn.assert_called_once_with("connection_object")
        assert conn == "final_wrapped_conn_object"
