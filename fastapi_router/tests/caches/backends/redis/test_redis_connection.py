from unittest.mock import Mock, AsyncMock

import pytest

from fastapi_router.caches.backends.redis.redis_connection import RedisConnectionPoolClient


class TestRedisConnection:

    @pytest.fixture
    def redis_conn_wrapper(self):
        connection = AsyncMock()
        return RedisConnectionPoolClient(connection), connection

    def test_constructor(self):
        redis_conn = Mock()
        conn = RedisConnectionPoolClient(redis_conn)
        assert conn._connection is redis_conn

    @pytest.mark.anyio
    async def test_flush_db(self, redis_conn_wrapper):
        await redis_conn_wrapper[0].flush_db()
        redis_conn_wrapper[1].flushdb.assert_awaited_once_with()

    @pytest.mark.anyio
    async def test_set_many(self, redis_conn_wrapper):
        data = Mock()
        await redis_conn_wrapper[0].set_many(data=data)
        redis_conn_wrapper[1].mset.assert_awaited_once_with(data)

    @pytest.mark.anyio
    async def test_get(self, redis_conn_wrapper):
        data = Mock()
        redis_conn_wrapper[1].get.return_value = "returned_result"
        result = await redis_conn_wrapper[0].get(key=data)
        redis_conn_wrapper[1].get.assert_awaited_once_with(data)
        assert result == "returned_result"

    @pytest.mark.anyio
    async def test_set(self, redis_conn_wrapper):
        key = Mock()
        value = Mock()
        await redis_conn_wrapper[0].set(key=key, value=value)
        redis_conn_wrapper[1].set.assert_awaited_once_with(name=key, value=value)

    @pytest.mark.anyio
    async def test_close_all_connections_in_pool(self, redis_conn_wrapper):
        await redis_conn_wrapper[0].close_all_connections_in_pool()
        redis_conn_wrapper[1].connection_pool.disconnect.assert_called_once_with(inuse_connections=True)
        assert not redis_conn_wrapper[0]._connection

    @pytest.mark.anyio
    async def test_close_stale_connections_in_pool(self, redis_conn_wrapper):
        await redis_conn_wrapper[0].close_stale_connections_in_pool()
        redis_conn_wrapper[1].connection_pool.disconnect.assert_awaited_once_with(inuse_connections=False)
        assert redis_conn_wrapper[0]._connection is redis_conn_wrapper[1]

    def test_get_internal_connection(self, redis_conn_wrapper):
        conn = redis_conn_wrapper[0].get_internal_connection()
        assert conn is redis_conn_wrapper[1]
