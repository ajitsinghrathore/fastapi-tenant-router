import pytest

from fastapi_router.context_injection.context_injector import TenantContextBind
from fastapi_router.context_injection.tenant_context import TenantContext
from fastapi_router.context_injection.tenant_context_tracker import tenant_context_tracker


class TestTenantContextInjector:

    @staticmethod
    @pytest.mark.anyio
    def test_context_binder():
        assert not tenant_context_tracker.current()
        context = TenantContext("tenant_id_1")
        with TenantContextBind(tenant_context=context) as tenant_context:
            assert tenant_context is context
            assert tenant_context_tracker.get_stack() == [context]
        assert tenant_context_tracker.get_stack() == []

    @pytest.mark.anyio
    async def test_nested_context_bind(self):
        assert not tenant_context_tracker.current()
        context = TenantContext("tenant_id_1")
        with TenantContextBind(tenant_context=context) as tenant_context:
            assert tenant_context is context
            assert tenant_context_tracker.get_stack() == [context]
            context_2 = TenantContext("tenant_id_2")
            with TenantContextBind(tenant_context=context_2) as tenant_context2:
                assert tenant_context2 is context_2
                assert tenant_context_tracker.get_stack() == [context, context_2]
            assert tenant_context_tracker.get_stack() == [context]
        assert tenant_context_tracker.get_stack() == []
