from fastapi_router.context_injection.tenant_context import TenantContext, TenantUpdateException


class TestTenantContext:

    def test_constructor(self):
        context = TenantContext(tenant_id="tenant_id")
        assert context.tenant_id == "tenant_id"

    def test_equal_tenant(self):
        context1 = TenantContext(tenant_id="id1")
        context2 = TenantContext(tenant_id="id1")

        assert context1 == context2

    def test_unequal_tenant(self):
        context1 = TenantContext(tenant_id="id1")
        context2 = TenantContext(tenant_id="id2")

        assert context1 != context2

    def test_str(self):
        context = TenantContext("tenant_id")
        assert str(context) == "tenant_id"

    def test_repr(self):
        context = TenantContext("tenant_id")
        assert repr(context) == "tenant_id"

    def test_set_attr_unsuccessfully(self):
        context = TenantContext(tenant_id="id")
        try:
            context.tenant_id = "id_updated"
            assert False
        except TenantUpdateException:
            assert True

    def test_set_new_attr_unsuccessfully(self):
        context = TenantContext(tenant_id="id")
        try:
            context.something = "id_updated"
            assert False
        except TenantUpdateException:
            assert True

    def test_delete_attr_unsuccessfully(self):
        context = TenantContext(tenant_id="id")
        try:
            del context.tenant_id
            assert False
        except TenantUpdateException:
            assert True
