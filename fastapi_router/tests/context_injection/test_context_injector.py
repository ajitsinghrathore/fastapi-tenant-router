import logging
from unittest.mock import Mock, patch

from fastapi_router.context_injection.context_injector import TenantContextBind


class TestTenantContextBind:

    def test_constructor(self):
        tenant_context = Mock()
        context_binder = TenantContextBind(tenant_context=tenant_context)

        assert context_binder._context == tenant_context

    @patch("fastapi_router.context_injection.context_injector.tenant_context_tracker")
    def test_enter(self, patched_tenant_context_tracker, caplog):
        caplog.set_level(logging.INFO)
        patched_tenant_context_tracker.get_stack.return_value = "stack"
        context = Mock()
        context_binder = TenantContextBind(tenant_context=context)

        received_context_from_enter = context_binder.__enter__()

        patched_tenant_context_tracker.push.assert_called_once_with(context)

        assert "tenant context injected successfully, current tenant stack is stack" in caplog.text
        assert received_context_from_enter == context

    @patch("fastapi_router.context_injection.context_injector.tenant_context_tracker")
    def test_exit(self, patched_tenant_context_tracker, caplog):
        caplog.set_level(logging.INFO)
        patched_tenant_context_tracker.get_stack.return_value = "stack"
        context = Mock()
        context_binder = TenantContextBind(tenant_context=context)

        context_binder.__exit__()

        patched_tenant_context_tracker.pop.assert_called_once()

        assert "tenant context popped successfully, current tenant stack is stack" in caplog.text
