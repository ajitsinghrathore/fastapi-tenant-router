import logging
from unittest.mock import patch, Mock

import pytest

from fastapi_router.context_injection.tenant_context_tracker import TenantContextTracker


class TestTenantContextTracker:

    @pytest.fixture
    @patch("fastapi_router.context_injection.tenant_context_tracker.ContextVar")
    def tenant_tracker(self, patched_context_var):
        return TenantContextTracker()

    @patch("fastapi_router.context_injection.tenant_context_tracker.ContextVar", return_value="context_var")
    def test_constructor(self, patched_context_var):
        tracker = TenantContextTracker()
        patched_context_var.assert_called_once_with("tenant_context_stack")
        assert tracker.tenant_context_stack == "context_var"

    def test_push_first_time(self, tenant_tracker):
        some_context = Mock()
        tenant_tracker.tenant_context_stack.get.return_value = []

        tenant_tracker.push(some_context)

        tenant_tracker.tenant_context_stack.get.assert_called_once_with([])
        tenant_tracker.tenant_context_stack.set.called_once_with([some_context])

    def test_push_not_first_time(self, tenant_tracker):
        some_context = Mock()
        tenant_tracker.tenant_context_stack.get.return_value = ["previous_context"]

        tenant_tracker.push(some_context)

        tenant_tracker.tenant_context_stack.get.assert_called_once_with([])
        tenant_tracker.tenant_context_stack.set.called_once_with(["previous_context", some_context])

    def test_pop_successfully(self, tenant_tracker):
        tenant_tracker.tenant_context_stack.get.return_value.pop.return_value = "popped_context"
        context = tenant_tracker.pop()

        assert context == "popped_context"

    @patch("fastapi_router.context_injection.tenant_context_tracker.sys")
    def test_pop_unsuccessfully_from_index_error(self, patched_sys, tenant_tracker, caplog):
        caplog.set_level(logging.DEBUG)
        tenant_tracker.tenant_context_stack.get.return_value = []
        patched_sys.exc_info = "exc_info"
        context = tenant_tracker.pop()

        assert not context
        assert "tenant pop was unsuccessful. error ==> exc_info" in caplog.text

    def test_current_successfully(self, tenant_tracker):
        tenant_tracker.tenant_context_stack.get.return_value = ["context1", "context2"]
        context = tenant_tracker.current()

        assert context == "context2"

    @patch("fastapi_router.context_injection.tenant_context_tracker.sys")
    def test_current_unsuccessfully_from_index_error(self, patched_sys, tenant_tracker, caplog):
        caplog.set_level(logging.DEBUG)
        tenant_tracker.tenant_context_stack.get.return_value = []
        patched_sys.exc_info = "exc_info"
        context = tenant_tracker.current()

        assert not context
        assert "tenant top lookup was unsuccessful. error ==> exc_info" in caplog.text

    def test_reset(self, tenant_tracker, caplog):
        caplog.set_level(logging.INFO)
        tenant_tracker.reset()

        tenant_tracker.tenant_context_stack.set.assert_called_once_with([])
        assert "tenant stack was reset" in caplog.text

    def test_get_stack(self, tenant_tracker):
        tenant_tracker.tenant_context_stack.get.return_value = ["ctx1", "ctx2"]

        stack = tenant_tracker.get_stack()

        assert stack == ["ctx1", "ctx2"]
        tenant_tracker.tenant_context_stack.get.assert_called_once_with([])
