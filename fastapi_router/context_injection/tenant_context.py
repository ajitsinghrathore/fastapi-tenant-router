from typing import Any


class TenantUpdateException(Exception):
    message = "this is an immutable object"


class TenantContext:
    """
    This class  is an representation of tenants  and helps
    to uniquely identify them with the help of tenant_id
    """

    def __init__(self, tenant_id: str) -> None:
        self.tenant_id = tenant_id

    def __eq__(self, o: object) -> bool:
        return self.tenant_id == getattr(o, "tenant_id")

    def __str__(self) -> str:
        return f"{self.tenant_id}"

    def __repr__(self) -> str:
        return f"{self.tenant_id}"

    def __setattr__(self, name: str, value: Any) -> None:
        if name in self.__dict__ or name != "tenant_id":
            raise TenantUpdateException()
        super().__setattr__(name, value)

    def __delattr__(self, name: str) -> None:
        raise TenantUpdateException()
