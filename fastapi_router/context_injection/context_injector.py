from logging import getLogger

from .tenant_context import TenantContext
from .tenant_context_tracker import tenant_context_tracker

logger = getLogger(__name__)


class TenantContextBind:
    """
    This class is used to switch  between tenants during a request lifecycle
    """

    def __init__(self, tenant_context: TenantContext) -> None:
        self._context = tenant_context

    def __enter__(self):
        tenant_context_tracker.push(self._context)
        logger.info(
            "tenant context injected successfully,"
            f" current tenant stack is {tenant_context_tracker.get_stack()}"
        )
        return self._context

    def __exit__(self, *args):
        tenant_context_tracker.pop()
        logger.info(
            "tenant context popped successfully, "
            f"current tenant stack is {tenant_context_tracker.get_stack()}"
        )
