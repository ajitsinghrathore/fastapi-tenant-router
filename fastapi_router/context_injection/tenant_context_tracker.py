import sys
from contextvars import ContextVar
from logging import getLogger
from typing import List

from .tenant_context import TenantContext

logger = getLogger(__name__)


class TenantContextTracker:
    """
    This class  is responsible for tracking all the tenants context
     used in a request lifecycle
    """

    def __init__(self) -> None:
        self.tenant_context_stack = ContextVar("tenant_context_stack")

    def push(self, tenant_context: TenantContext) -> None:
        stack: List[TenantContext] = self.tenant_context_stack.get([])
        if not stack:
            self.tenant_context_stack.set([tenant_context])
        else:
            stack.append(tenant_context)
            self.tenant_context_stack.set(stack)

    def pop(self) -> TenantContext | None:
        try:
            return self.tenant_context_stack.get([]).pop()
        except IndexError:
            logger.debug(
                f"tenant pop was unsuccessful. error ==> {sys.exc_info}"
            )
            return None

    def current(self) -> TenantContext | None:
        try:
            return self.tenant_context_stack.get([])[-1]
        except IndexError:
            logger.debug(
                f"tenant top lookup was unsuccessful. error ==> {sys.exc_info}"
            )
            return None

    def reset(self) -> None:
        self.tenant_context_stack.set([])
        logger.info("tenant stack was reset")

    def get_stack(self) -> List[TenantContext]:
        return self.tenant_context_stack.get([])


tenant_context_tracker = TenantContextTracker()
