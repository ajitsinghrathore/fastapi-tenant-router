#### get_cache("cache alias"")

This will return the GeneralCachePoolClient for a tenant with cache alias given in parameter. if no alias is given then
it will take default cache alias defined in settings .

#### TenantContextBind

TenantContextBind is a context manager used to switch between different tenant contexts . Suppose while executing a
request for a tenant you may want to execute a query in different tenant context and then back to the original tenant
context . This context manager do the same thing, it will take tenant context as its parameter and then any database
query or cache query executed will be connected to the new tenant's context and after exiting the context manager all
query will get routed to its own tenant

As an example ,

```

## first_tenant

with TenantContextBind(tenant_context= TenantContext("second_tenant")):
        ## execute query in second_tenant context


# execute query in first_tenant context         

```

#### TenantContext

This is a class used to represent a tenant context, it can be instantiated by passing tenant id to its constructor .
This class is immutable i.e. new attributes can not be added and existing one can not be modified or changed.

```
   context = TenantContext("tenant_id") 
```