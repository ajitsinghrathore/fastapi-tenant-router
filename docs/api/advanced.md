#### tenant_context_tracker

This object can be used to push tenant context , pop tenant contexts manually or for checking the current tenant context
stack.  **it is not advised to push or pop contexts manually using this** instead use *TenantContextBind*
for that , as doing manually may produce some bugs if push and pop are not done properly .

##### get_stack() -> List[TenantContext]

    return the current stack of tenant contexts

##### current() -> TenantContext | None

    return the current tenant context

##### pop() -> TenantContext | None

    return the latest tenant context pushed 

##### push(tenant_context: TenantContext) -> None:

    pushes the tenant context to the existing stack 

#### pre_request and post_request hooks

For registering a pre request or post request hook you can use TenantContextMiddleware class . The callback must be an
async function . **request started callback will not execute in any tenant context**, it can be registered as shown
below

```

# function is not called , just pass the reference 
TenantContextMiddleware.add_request_finished_coroutine(function_reference)

TenantContextMiddleware.add_request_started_coroutine(function_reference)
```


