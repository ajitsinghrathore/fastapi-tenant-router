### MiddlewareSettings

    - tenant_id_resolver: Optional[Callable]
    - whitelist_routes: Optional[List[str]] = []

#### tenant_id_resolver

tenant id resolver is a optional callable through which you can change the default way of resolving tenant id , By
default the middleware of this library will check for ```x-tenant-id```  header and identify each tenant according to
that header , but if you want different mechanism then provide a callable which when called should return the tenant id
for that particular request .

Callable should be defined as shown below

```
# it will recieve fastapi request object as its parameter and
# should return tenant id for that request 
def tenant_id_resolver(request: Request) -> str:
    pass 

```

#### whitelist_routes

whitelist routes are the routes which do not execute in any tenant context or any route which do not need any tenant
context . By default, library will try to resolve tenant id for each request and on failure it will raise an error and
return without executing any endpoints . So if you want to change this behaviour then list all routes to be whitelisted
.

Now we can add these settings in our main settings as shown below

```

# the main setting object which will hold all the settings and connection configurations
settings: FastApiTenantRouterSettings = FastApiTenantRouterSettings(
    # .... all other settings 
    
    
    # middleware_settings
    middleware_settings = MiddlewareSettings(
        tenant_id_resolver = "path to tenant id resolver callable",
        whitelist_routes = ["/docs", "any other whitelisted path"]
    )
)

```