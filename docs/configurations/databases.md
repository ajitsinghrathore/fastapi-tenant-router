Each tenant can also connect to multiple databases , So for each orm settings we can give all the connections with
different alias .

---
**NOTE**

As of now this library only supports [Tortoise Orm](https://tortoise-orm.readthedocs.io/)

---

### DatabaseSettings

    - tortoise : Optional[TortoiseOrmSetting]

### TortoiseOrmSetting

    - apps: Dict[str, Dict]
    - routers: Optional[List[str]] = []
    - add_exception_handlers: Optional[bool] = True
    - use_tz: Optional[bool] = False
    - timezone: Optional[str] = "UTC"

For more info on above parameters see tortoise orm documentation , as they are exactly the same

#### Routers for tortoise orm

When each tenant is having multiple databases, then by looking at the header this library will identify the correct
tenant but inside that tenant also there are multiple databases so which one to connect is a question . And this
question is answered with the help of routers defined by you. These routers will return the local alias of connection to
use for a given model (same as normal tortoise router).

So there will be no change in routers code also, as the expected return value is one of local alias only.

Now we can add these settings in our main settings as shown below

```

# the main setting object which will hold all the settings and connection configurations
settings: FastApiTenantRouterSettings = FastApiTenantRouterSettings(

    # database settings 
    database_settings=DatabaseSettings(

        # tortoise orm settings goes here
        tortoise=TortoiseOrmSetting(
            apps={
                "app_1": {
                    "models": ['path.to.models_module'],

                    # default tortoise orm connection (you just need to provide the local alias as you would have 
                    # done in single tenant application )
                    "default_connection": "db_1"
                }
            },
            routers: ["path to router class"],
        )
    ),
)

```