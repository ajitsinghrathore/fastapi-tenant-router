Each Tenant can connect to multiple caches so for each tenant you can give multiple cache connection string with
different alias . And these caches can be accessed using public api ```get_cache("alias_name")``` provided . where
alias_name is the alias for the connection which we want and if no alias is given then, for that we can give default
cache alias to use , in the settings .

## CacheSettings

    - default_cache_alias : Optional[str] = "default"

So by default the default alias is default, and you change that by passing the object of CacheSettings to the main
settings objects .

```

# the main setting object which will hold all the settings and connection configurations
settings: FastApiTenantRouterSettings = FastApiTenantRouterSettings(
    # ...... all other settings
    
    # cache settings 
    cache_settings = CacheSettings(default_cache_alias="new_default_alias")

)

```