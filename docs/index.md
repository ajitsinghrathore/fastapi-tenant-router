FastApi tenant router is a python library designed for Fastapi to convert a single tenant application into multi tenant
application (using isolated database approach ) with minimal change in codebase, basically once you have given the
configuration to fastapi tenant router then library will take care of connecting to correct database.

let's consider an example of single tenant application , that you operate on data of students from multiple colleges and
for each college you have separate server and database something like illustrated below

[![Architecture](https://drive.google.com/uc?export=view&id=1vFQhKp5u6jP59ZNnDpZNq7Zc7qlV0c_C)](https://drive.google.com/file/d/1vFQhKp5u6jP59ZNnDpZNq7Zc7qlV0c_C/view?usp=sharing)

and the basic model of data is students basic information, its model will look like

```

class Students(SomeOrmLibraryBaseModel):
    # all the fields
    
```

and then the basic api for CRUD (using tortoise orm) would be

```

# for fetching information of all students of that college 
@app.get("/students/", response_model=List[students_pydantic])
async def read_users():
    users = await Students.all()
    return users




# for adding a new student information 
@app.post("/students/", response_model=students_pydantic, status_code=status.HTTP_201_CREATED)
async def read_users(student: students_pydantic):
    obj = await Students.create(**Student.dict())
    return obj

```

now you want to convert your single tenant application into multi tenant using isolated database , as illustrated below

[![Architecture](https://drive.google.com/uc?export=view&id=1yPGlnyS7BK749NNynWMBTjC8QcQ9iS_k)](https://drive.google.com/file/d/1yPGlnyS7BK749NNynWMBTjC8QcQ9iS_k/view?usp=sharing)

Then for achieving this , ideally you need to write some logic of connecting to the proper database and execute the
query using that database , but with the help of this library you don't need to change a single line of code in your api
endpoints , just need to configure it at a single point and you are done .

so the endpoints code will be exactly same

```

# for fetching information of all students of that college 
@app.get("/students/", response_model=List[students_pydantic])
async def read_users():
    users = await Students.all()
    return users




# for adding a new student information 
@app.post("/students/", response_model=students_pydantic, status_code=status.HTTP_201_CREATED)
async def read_users(student: students_pydantic):
    obj = await Students.create(**Student.dict())
    return obj

```

here all the orm query will be routed internally by the library to the correct database using the tenant id provided in
header .
---
**NOTE**
This library differentiates each tenant through different tenant id provided in header , it does not authenticate that
claim , it will assume it to be authenticated user.
---


