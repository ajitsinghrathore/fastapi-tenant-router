Firstly define the connections config dictionary which will keep the information of how many tenants are in your
application and how many databases or caches are there per tenant and how to connect them

```

all_config = {

    # identifying tenant id
    "tenant_1": {

        # all the database configuration goes in this nested dictionary
        "databases": {

            # all the tortoise orm connections for a particular tenant goes here
            FastApiTenantRoutersORM.tortoise: {

                # local database alias (can be same for all tenant but locally needs to be unique)
                "db_1": {

                    # connection string 
                    "conn": f"postgres://username:password@127.0.0.1:9000/fast_api_testing_database_tenant_1_db_1",
                },
                "db_2": {
                    "conn": f"postgres://username:password@127.0.0.1:9000/fast_api_testing_database_tenant_1_db_2",
                }
            },
        },

        # all the caches configuration goes in this nested dictionary
        "caches": {

            # local cache alias (can be same for all tenant but locally needs to be unique)
            "default": {

                # connection string 
                "conn": "redis://127.0.0.1:9500/1",
                # backend to use redis or memcached
                "backend": "redis"
            },
            "cache_alias_2": {
                "conn": "redis://127.0.0.1:9500/2",
                "backend": "redis"
            }
        }
    }
}
```

Now that we defined all the connections , lets define some other basic configurations like middleware settings and ORM
settings . In this tutorial we will take very basic settings as an example for more advanced options check the
Configuration documentation of that particular setting

```

# the main setting object which will hold all the settings and connection configurations
settings: FastApiTenantRouterSettings = FastApiTenantRouterSettings(

    # database settings 
    database_settings=DatabaseSettings(

        # tortoise orm settings goes here
        tortoise=TortoiseOrmSetting(
            apps={
                "app_1": {
                    "models": ['path.to.models_module'],

                    # default tortoise orm connection (you just need to provide the local alias as you would have 
                    # done in single tenant application )
                    "default_connection": "db_1"
                }
            },

        )
    ),

    # this will contain all the connections config dictionary defined above
    all_tenants_config=AllTenantConfig(tenants_metadata=all_config)

)

```

In next section we will see how to use this settings 
