Now that we have defined our settings lets see how to integrate fastapi router library with your existing or new Fastapi
application .

For integrating you just need to call a callable object as shown below

```

# importing the  callable 
from fastapi_router import fastapi_multi_tenant_router_set_app



# here app is the fastapi app variable and settings is the main settings object which we have defined in settings 
# section
fastapi_multi_tenant_router_set_app(app, settings)



```

And the rest of your endpoints will remain unchanged .

```

# for fetching information of all students of that college 
@app.get("/students/", response_model=List[students_pydantic])
async def read_users():
    users = await Students.all()
    return users




# for adding a new student information 
@app.post("/students/", response_model=students_pydantic, status_code=status.HTTP_201_CREATED)
async def read_users(student: students_pydantic):
    obj = await Students.create(**Student.dict())
    return obj


```