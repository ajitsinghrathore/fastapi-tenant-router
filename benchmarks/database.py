import asyncio

import asyncpg
import typer
from asyncpg import DuplicateDatabaseError

app = typer.Typer()


async def create_table(connection_uri: str, username: str):
    connection: asyncpg.connection = await asyncpg.connect(connection_uri)
    try:
        async with connection.transaction():
            await connection.execute("""
                           CREATE TABLE IF NOT EXISTS "users" (
                                "id" SERIAL NOT NULL PRIMARY KEY,
                                "name" VARCHAR(25) NOT NULL
                            );
                    """)
            await connection.execute(f"""
                INSERT INTO users(id, name)
                VALUES (1, '{username}');
            """)
            typer.echo(f"initialized table  for {connection_uri}")
    except:
        print("something wrong in creating table")

    await connection.close()


async def create_database(pool: asyncpg.pool, database_name: str):
    try:
        await pool.execute(f"CREATE DATABASE {database_name};")
        typer.echo(f"database created successfully {database_name}")
    except DuplicateDatabaseError:
        typer.style(f"database {database_name} already exists .", fg=typer.colors.WHITE, bg=typer.colors.RED)


async def start_creating_databases(
        connection_uri: str,
        number_of_tenants: int,
        number_of_database_per_tenant: int,
        database_alias_template: str
):
    pool: asyncpg.Pool = await asyncpg.create_pool(connection_uri, min_size=1, max_size=100)
    requests = []

    for number in range(1, number_of_tenants + 1):
        for alias_number in range(1, number_of_database_per_tenant + 1):
            database_name = database_alias_template.format(tenant_number=number, alias_number=alias_number)
            requests.append(create_database(pool, database_name))

    await asyncio.gather(*requests)

    typer.echo("completed creation of databases")
    try:
        await asyncio.wait_for(pool.close(), 10)
    except TimeoutError:
        await pool.terminate()

    typer.echo("connection pool closed")

    requests.clear()

    for number in range(1, number_of_tenants + 1):
        for alias_number in range(1, number_of_database_per_tenant + 1):
            database_name = database_alias_template.format(tenant_number=number, alias_number=alias_number)
            requests.append(create_table(f"{connection_uri}/{database_name}", f"tenant_{number}_alias_{alias_number}"))
            if len(requests) == 10:
                await asyncio.gather(*requests)
                requests.clear()

    await asyncio.gather(*requests)
    requests.clear()

    typer.echo("completed table creation")


@app.command()
def create():
    connection_uri: str = typer.prompt("connection uri ", default="postgresql://ajit:password@localhost:9000")
    number_of_tenants: int = typer.prompt("number of tenants", default=10000)
    number_of_database_per_tenant: int = typer.prompt("number_of_databases for each tenant", default=2)
    database_alias_template: str = typer.prompt(
        "database alias template to use",
        default="fast_api_testing_database_tenant_{tenant_number}_db_{alias_number}"
    )
    # asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    asyncio.run(
        start_creating_databases(
            connection_uri,
            number_of_tenants,
            number_of_database_per_tenant,
            database_alias_template
        )
    )


async def drop_database(pool: asyncpg.connection, database_name: str):
    await pool.execute(f"DROP DATABASE IF EXISTS {database_name}  WITH (FORCE);")
    typer.echo(f"dropped database {database_name}")


async def start_dropping_databases(connection_uri: str):
    pool: asyncpg.Pool = await asyncpg.create_pool(connection_uri, min_size=1, max_size=100)
    all_database = []

    result = await pool.fetch("select datname from pg_database where datname LIKE 'fast_api_testing_database_%';")

    all_database = [drop_database(pool, record['datname']) for record in result]

    await asyncio.gather(*all_database)

    typer.echo("completed dropping of databases")
    try:
        await asyncio.wait_for(pool.close(), 10)
    except TimeoutError:
        await pool.terminate()
    typer.echo("connection closed")


@app.command()
def drop():
    connection_uri: str = typer.prompt("connection uri ", default="postgresql://ajit:password@localhost:9000")
    asyncio.run(start_dropping_databases(connection_uri))


if __name__ == "__main__":
    app()
