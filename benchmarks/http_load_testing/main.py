import logging
from typing import List

from fastapi import FastAPI, HTTPException
from starlette import status

from fastapi_router import fastapi_multi_tenant_router_set_app
from fastapi_router.context_injection.context_injector import TenantContextBind
from fastapi_router.context_injection.tenant_context import TenantContext
from .models import users_pydantic, Users
from .settings import settings

logging.basicConfig(filename='http_load_test.log', level=logging.DEBUG)

app = FastAPI()

fastapi_multi_tenant_router_set_app(app, settings)


@app.get("/users/", response_model=List[users_pydantic])
async def read_users():
    users = await Users.all()
    return users


@app.post("/users/", response_model=users_pydantic, status_code=status.HTTP_201_CREATED)
async def read_users(user: users_pydantic):
    try:
        obj = await Users.create(**user.dict())
        return obj
    except Exception as e:
        print(e)
        raise HTTPException(status_code=status.HTTP_206_PARTIAL_CONTENT, detail="some error occurred")
