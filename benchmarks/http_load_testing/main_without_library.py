import logging
from typing import List

from fastapi import FastAPI, HTTPException
from starlette import status
from tortoise.contrib.fastapi import register_tortoise

from .models import users_pydantic, Users

logging.basicConfig(filename='http_load_test.log', level=logging.DEBUG)

app = FastAPI()


register_tortoise(
    app,
    db_url="postgres://ajit:password@0.0.0.0:9000/fast_api_testing_database_tenant_1_db_1",
    modules={"models": ['benchmarks.http_load_testing.models']},
    generate_schemas=False,
    add_exception_handlers=True,
)


@app.get("/users/", response_model=List[users_pydantic])
async def read_users():
    users = await Users.all()
    return users


@app.post("/users/", response_model=users_pydantic, status_code=status.HTTP_201_CREATED)
async def read_users(user: users_pydantic):
    try:
        obj = await Users.create(**user.dict())
        return obj
    except Exception as e:
        print(e)
        raise HTTPException(status_code=status.HTTP_206_PARTIAL_CONTENT, detail="some error occurred")
