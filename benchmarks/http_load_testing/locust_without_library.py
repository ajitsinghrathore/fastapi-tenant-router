from random import randint

from locust import HttpUser, events, task, between


@events.init.add_listener
def initialize_tenant_ids_range(**kwargs):
    User.total_tenants = 1000


class User(HttpUser):
    total_tenants: int = 1
    host = "http://127.0.0.1:8000"
    wait_time = between(1, 5)

    def _get_header_with_random_tenant_id(self):
        tenant_id = randint(1, self.__class__.total_tenants)
        return {
                   "x-tenant-id": f"tenant_{tenant_id}"
               }, tenant_id

    @task
    def normal_get_request(self):
        header, tenant_number = self._get_header_with_random_tenant_id()
        with self.client.get("/users/", headers=header, catch_response=True) as response:
            if response.status_code != 200:
                response.failure(" not received 200 status code")
            else:
                res_json = response.json()
                if res_json == [{
                    "id": 1,
                    "name": f"tenant_1_alias_1"
                }]:
                    response.success()
                else:
                    response.failure("received wrong data")
