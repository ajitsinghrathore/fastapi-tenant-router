from tortoise.contrib.pydantic import pydantic_model_creator
from tortoise.fields import CharField, IntField
from tortoise.models import Model


class Users(Model):
    id = IntField(pk=True)
    name = CharField(max_length=25)


users_pydantic = pydantic_model_creator(cls=Users)
