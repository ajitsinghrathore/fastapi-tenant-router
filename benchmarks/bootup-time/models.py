from tortoise.fields import CharField, IntField
from tortoise.models import Model


class Users(Model):
    id = IntField(pk=True)
    name = CharField(max_length=25)
