import logging

from fastapi import FastAPI

from fastapi_router import fastapi_multi_tenant_router_set_app
from .settings import settings

logging.basicConfig(filename='boot-up_time_test.log', level=logging.DEBUG)

app = FastAPI()

fastapi_multi_tenant_router_set_app(app, settings)
