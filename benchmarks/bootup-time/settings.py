from fastapi_router.config.all_tenant_config import FastApiTenantRouterSettings, AllTenantConfig
from fastapi_router.config.database import DatabaseSettings, TortoiseOrmSetting, FastApiTenantRoutersORM
from fastapi_router.config.middleware import MiddlewareSettings

all_config = {}

for i in range(1, 1000 + 1):
    config = {
        "databases": {
            FastApiTenantRoutersORM.tortoise: {
                "db_1": {
                    "conn": f"postgres://ajit:password@127.0.0.1:9000/fast_api_testing_database_tenant_{i}_db_1",
                },
                "db_2": {
                    "conn": f"postgres://ajit:password@127.0.0.1:9000/fast_api_testing_database_tenant_{i}_db_2",
                }
            },
        },
        "caches": {
            "default": {
                "conn": "redis://127.0.0.1:9500/1",
                "backend": "redis"
            },
            "cache_alias_2": {
                "conn": "redis://127.0.0.1:9500/2",
                "backend": "redis"
            }
        }
    }

    all_config[f"tenant_{i}"] = config

settings: FastApiTenantRouterSettings = FastApiTenantRouterSettings(
    database_settings=DatabaseSettings(
        tortoise=TortoiseOrmSetting(
            apps={
                "app_1": {
                    "models": ['benchmarks.bootup-time.models'],
                    "default_connection": "db_1"
                }
            },

        )
    ),
    middleware_settings=MiddlewareSettings(whitelist_routes=["/docs", "/openapi.json"]),
    all_tenants_config=AllTenantConfig(tenants_metadata=all_config)
)
